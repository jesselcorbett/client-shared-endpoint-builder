'use strict';

module.exports = function (grunt) {

    // Load grunt tasks automatically
    require('load-grunt-tasks')(grunt);

    // Time how long tasks take. Can help when optimizing build times
    require('time-grunt')(grunt);

    require('grunt-karma')(grunt);

    // Define the configuration for all the tasks
    grunt.initConfig({
        bower: grunt.file.readJSON('bower.json'),

        // Project settings
        yeoman: {
            // configurable paths
            app: 'src',
            test: 'test',
            dist: 'dist'
        },


        // Watches files for changes and runs tasks based on the changed files
        watch: {
            bower: {
                files: ['bower.json'],
                tasks: ['requirejs', 'newer:copy:app']
            },
            js: {
                files: ['<%= yeoman.app %>/**/*.js'],
                tasks: ['newer:copy:app', 'newer:jshint:all']
            },
            gruntfile: {
                files: ['Gruntfile.js'],
                tasks: ['newer:copy:app']
            },
            jsTest: {
                files: ['test/spec/**/*.js', 'test/mocks/**/*.js'],
                tasks: ['newer:jshint:test', 'karma']
            },
            jscs : {
                files: ['test/spec/**/*.js', 'test/mocks/**/*.js', '<%= yeoman.app %>/**/*.js'],
                tasks: ['jscs']
            }
        },

        // The actual grunt server settings
        connect: {
            options: {
                port: 9000,
                // Change this to '0.0.0.0' to access the server from outside.
                hostname: '0.0.0.0'
            },
            dist: {
                options: {
                    base: '<%= yeoman.dist %>'
                }
            },
            coverage: {
                options: {
                    port: 9002,
                    open: true,
                    base: ['coverage']
                }
            }
        },

        // Make sure code styles are up to par and there are no obvious mistakes
        jshint: {
            options: {
                jshintrc: '.jshintrc',
                reporter: require('jshint-stylish')
            },
            all: [
                'Gruntfile.js',
                '<%= yeoman.app %>/**/*.js'
            ],
            test: {
                options: {
                    jshintrc: 'test/.jshintrc'
                },
                src: ['test/spec/**/*.js']
            }
        },

        jscs: {
            src: '<%= yeoman.app %>/**/*.js'
        },

        // Empties folders to start fresh
        clean: {
            dist: {
                files: [{
                    dot: true,
                    src: [
                        '.temp',
                        '<%= yeoman.dist %>/*',
                        '!<%= yeoman.dist %>/.git*'
                    ]
                }]
            },
            server: '.temp'
        },


        // Copies remaining files to places other tasks can use
        copy: {
            app: {
                expand: true,
                cwd: '<%= yeoman.app %>',
                dest: '<%= yeoman.dist %>/',
                src: [
                    '**/*',
                    '!**/*.(scss,sass,css)'
                ]
            }
        },

        // Test settings
        // These will override any config options in karma.conf.js if you create it.
        karma: {
            unit: {
                configFile: 'test/karma.conf.js'
            }
        },

        // ngAnnotate tries to make the code safe for minification automatically by
        // using the Angular long form for dependency injection.
        ngAnnotate: {
            dist: {
                files: [{
                    expand: true,
                    cwd: '.temp/concat/scripts',
                    src: '*.js',
                    dest: '.temp/concat/scripts'
                }]
            }
        },

        requirejs: {
            compile: {
                options: {
                    baseUrl: '<%= yeoman.app %>',
                    mainConfigFile: '<%= yeoman.app %>/main.js',
                    name: 'main', // assumes a production build using almond
                    out: '<%= yeoman.dist %>/<%= bower.name %>.js',
                    optimize: 'none',
                    useStrict: true,
                    paths: {
                        angular: 'empty:'
                    }
                }
            }
        },

        sass: {
            dist: {
                files: {
                    '<%= yeoman.app %>/styles/mobilgov_pa.css' : '<%= yeoman.app %>/styles/mobilgov_pa.scss'
                }
            }
        },

        bowerRequirejs:
        {
            all: {
                rjsConfig: '<%= yeoman.app %>/components/main.js'
            },
            options: {
                transitive: true
            }
        },

        bump: {
            options: {
                files: ['package.json', 'bower.json'],
                commit: false,
                createTag: false,
                push: false
            }
        }
    });

    grunt.registerTask('test', [
        'clean',
        'newer:copy:app',
        'karma:unit:start'
    ]);

    grunt.registerTask('init', [
        'clean',
        'jscs',
        'sass',
        'concurrent:server',
        'newer:copy:app'
    ]);

    grunt.registerTask('compress', [
        'clean',
        'jscs',
        'requirejs'
    ]);

    grunt.registerTask('coverage',
        ['karma:continuous',
            'connect:coverage:keepalive'
        ]);

    grunt.registerTask('default', [
        'newer:jshint',
        'jscs',
        'karma:continuous',
        'compress'
    ]);

    grunt.registerTask('build', [
        'clean',
        'test',
        'jshint',
        'compress'
    ]);
};
