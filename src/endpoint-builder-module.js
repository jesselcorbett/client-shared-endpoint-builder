'use strict';

define(['angular'], function(angular) {
    return angular.module('mobilgov.endpoint-builder', ['mobilgov.environment-info']);
});
