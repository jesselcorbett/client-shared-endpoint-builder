'use strict';

define([
    './endpoint-builder-module',
    './endpoint-builder-service'
], function(module) {
    return module;
});
