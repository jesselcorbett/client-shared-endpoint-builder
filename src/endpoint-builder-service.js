'use strict';

define(['./endpoint-builder-module'], function(analyticsModule) {
    analyticsModule.factory('EndpointBuilder', ['$http', '$q', 'Logger', 'AuthCredentialProvider',
        function($http, $q, serviceConstants, Logger, AuthCredentialProvider) {

            return {

                /**
                 *
                 * @param url               URL for endpoint. Can contain String templates to be replaced later for URL parameters
                 * @param doAuth            Boolean value for if the user's auth token should be included in the headers
                 * @param optionalOptions   Object containing properties to be overwritten in the endpoint. Possible values include:
                 *                              dataType, contentType
                 * @returns endpoint object
                 */
                buildEndpoint: function(url, doAuth, optionalOptions) {
                    var endpoint = {};
                    endpoint.url = url;
                    endpoint.doAuth = doAuth;
                    endpoint.dataType = 'json';
                    endpoint.contentType = 'application/json';
                    endpoint.calls = [];

                    if (optionalOptions !== undefined) {
                        for (var key in optionalOptions) {
                            if (optionalOptions.hasOwnProperty(key)) {
                                endpoint[key] = optionalOptions[key];
                            }
                        }
                    }

                    /**
                     *
                     * @param pathParams        Object with keys reflecting strings to replace with corresponding values
                     * @param callbackSuccess   Function to call on success. Passes response data as parameter
                     * @param callbackFailure   Function to call on failure. Passes response data as parameter
                     * @param force             Force a GET call even if data already exists for identical call is being made
                     */
                    endpoint.get = function(pathParams, callbackSuccess, callbackFailure, force) {

                        //Build the call descriptor object
                        var getCall = {};
                        getCall.inProgress = true;
                        getCall.result = undefined;
                        getCall.callbackSuccesses = [callbackSuccess];
                        getCall.callbackFailures = [callbackFailure];
                        getCall.config = {};
                        getCall.config.processedUrl = url;
                        for (var key in pathParams) {
                            if (pathParams.hasOwnProperty(key)) {
                                getCall.config.processedUrl = getCall.config.processedUrl.replace(key, pathParams[key]);
                            }
                        }

                        // Check for and handle the case that an identical call has been made before, unless we're forcing the call
                        if (force !== true) {
                            for (var i = 0; i < endpoint.calls.length; i++) {
                                if (getCall.config === endpoint.calls[i].config) {
                                    // Call has been made before and has finished
                                    if (endpoint.calls[i].inProgress === false) {
                                        callbackSuccess(endpoint.calls[i].result);
                                        return;
                                        // Call has been made before, but is still in progress
                                    } else if (endpoint.calls[i].inProgress === true) {
                                        endpoint.calls[i].callbackSuccesses.push(callbackSuccess);
                                        endpoint.calls[i].callbackFailures.push(callbackFailure);
                                        return;
                                    }
                                }
                            }
                        }

                        // Generate call config
                        var httpOptions = {
                            url: endpoint.url,
                            dataType: endpoint.dataType,
                            method: 'GET',
                            headers:{
                                'Content-Type': endpoint.contentType
                            }
                        };
                        if (endpoint.doAuth) {
                            httpOptions.Authorization = AuthCredentialProvider.getToken();
                        }

                        // Make call and pass results to all functions requesting it
                        $http(httpOptions).success(function(data) {
                            getCall.result = data;
                            getCall.inProgress = false;
                            for (var x = 0; x < getCall.callbackSuccesses.length; x++) {
                                getCall.callbackSuccesses[x](data);
                            }
                        }).error(function(data) {
                            Logger.error('Failed to make api GET call for url', getCall.config.processedUrl);
                            for (var x = 0; x < getCall.callbackFailures.length; x++) {
                                getCall.callbackFailures[x](data);
                            }
                            // Remove this call from the list, since failed calls are useless to remember
                            for (var i = 0; i < endpoint.calls.length; i++) {
                                if (getCall.config === endpoint.calls[i].config) {
                                    endpoint.calls.slice(i);
                                }
                            }
                        });
                    };

                    /**
                     *
                     * @param pathParams        Object with keys reflecting strings to replace with corresponding values
                     * @param postData          Object containing POST data
                     * @param callbackSuccess   Function to call on success. Passes response data as parameter
                     * @param callbackFailure   Function to call on failure. Passes response data as parameter
                     */
                    endpoint.post = function(pathParams, postData, callbackSuccess, callbackFailure) {
                        var processedUrl = url;
                        for (var key in pathParams) {
                            if (pathParams.hasOwnProperty(key)) {
                                processedUrl = processedUrl.replace(key, pathParams[key]);
                            }
                        }
                        var httpOptions = {
                            data: postData,
                            url: endpoint.url,
                            dataType: endpoint.dataType,
                            method: 'POST',
                            headers:{
                                'Content-Type': endpoint.contentType
                            }
                        };
                        if (doAuth) {
                            httpOptions.Authorization = AuthCredentialProvider.getToken();
                        }

                        $http(httpOptions).success(function(data) {
                            callbackSuccess(data);
                        }).error(function(data) {
                            Logger.error('Failed to make api POST call for url', processedUrl);
                            if (callbackFailure !== undefined) {
                                callbackFailure(data);
                            }
                        });
                    };

                    return endpoint;
                }
            };
        }]);
});
