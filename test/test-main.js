'use strict';

var specFiles = null;
var baseUrl = '';
var requirejsCallback = null;

// if invoked in karma-runner environment
if (typeof window != 'undefined' && window.__karma__ != undefined) {
    // Karma serves files from '/base'
    var baseUrl = '/base';
    requirejsCallback = window.__karma__.start;

    // looking for *_spec.js files
    specFiles = [];
    for (var file in window.__karma__.files) {
        if (window.__karma__.files.hasOwnProperty(file)) {
            if (/.*\/test\/spec\/.+spec\.js$/.test(file)) {
                specFiles.push(file);
            }
        }
    }
}

require.config({
    baseUrl: '/base/src',
    paths: {
        'angular': '../bower_components/angular/angular',
        'angular-mocks': '../bower_components/angular-mocks/angular-mocks',
        'chai': '../bower_components/chai/chai',
        'mgovGlobalState': '../test/mocks/mgovGlobalState'
    },
    packages: [
        {
            name: 'mocks',
            location: '../../test/mocks'
        }
    ],
    shim: {
        'angular': {
            exports: 'angular'
        },
        'angular-mocks': ['angular'],
        'mocha': {
            exports: 'mocha'
        }
    },
    deps: specFiles,

    callback: requirejsCallback
});
