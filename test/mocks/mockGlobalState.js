
define([], function() {
    var mockGlobalState = {
        isOnWifi: false,
        isDeviceOnline: false,
        isFirstTimeOpeningApp: false,
        isProd: false,
        deviceType: '',
        customerID: '',
        token: '',
        tokenExpireDate: '',
        hasError: false,
        errorMessages: [],
        doneWithUpdate: false,
        showTutorial: true,
        appVersion: '',
        appName: ''
    };

    return mockGlobalState;
});
